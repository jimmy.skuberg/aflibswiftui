//
//  File.swift
//  
//
//  Created by piyawat kunama on 1/6/2565 BE.
//

import Foundation
import SwiftUI

@available(iOS 13.0.0, *)
public func Header (appKey: String, adKey: String) -> some View {
    return renderAd(appKey: appKey, adKey: adKey,adType: "header")
}
