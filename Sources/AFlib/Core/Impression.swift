import Foundation

func Impression (adServerKey:String,appKey:String,adKey:String,completion: @escaping ([String: Any]?, Error?) -> Void) {
    
    let json: [String: Any] = ["adServerKey":adServerKey, "appKey": appKey,"adKey": adKey]
    let jsonData = try? JSONSerialization.data(withJSONObject: json)
    let url = URL(string: "https://api-afbrother.skuberg.pro/creatives/app_ad_impression")!
    let session = URLSession.shared
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.httpBody = jsonData
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")
    
    let task = session.dataTask(with: request, completionHandler: { data, response, error in
        
        guard error == nil else {
            completion(nil, error)
            return
        }
        
        guard let data = data else {
            completion(nil, NSError(domain: "dataNilError", code: -100001, userInfo: nil))
            return
        }
        
        do {
            //create json object from data
            guard try JSONSerialization.jsonObject(with: data, options: .mutableContainers) is [String: Any] else {
                completion(nil, NSError(domain: "invalidJSONTypeError", code: -100009, userInfo: nil))
                return
            }
            
        } catch let error {
            
            print(error)
        }
    })
    
    task.resume()
}
