
import Foundation
import Combine

@available(iOS 13.0, *)
class ImageLoader: ObservableObject {
    var didChange = PassthroughSubject<Data, Never>()
    var adUrl = "https://lh3.googleusercontent.com/a-/AOh14GhDcCB1IHwnRS4RkGxXQOu_Z_R5px7G1sjHEBE7EA=s576-p-rw-no"
    var data = Data() {
        didSet {
            didChange.send(data)
        }
    }
    var width :Double = 0
    var height :Double = 0
    var href :String = ""
    
    init(appKey: String, adKey: String) {
        
        if (appKey=="close"&&adKey=="close"){
            guard let url = URL(string: "https://api-afbrother.skuberg.pro/api/serverImage/close.png") else { return }
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data else { return }
                DispatchQueue.main.async {
                    self.width = 20
                    self.height = 20
                    self.href = "close"
                    self.data = data
                }
            }
            task.resume()
        }else{
            let group = DispatchGroup()
            group.enter()
            getAFData(appKey: appKey, adKey:adKey) { (result, error) in
                if let result = result {
                    var url:String = ""
                    var width:Double = 0
                    var height:Double = 0
                    var href:String = ""
                    var key:String = ""
                    for index in result {
                        if(index.key == "url"){
                            url = index.value as! String
                        }
                        if(index.key == "width"){
                            let dataWidth = index.value
                            width = dataWidth as! Double
                        }
                        if(index.key == "height"){
                            let dataHeight = index.value
                            height = dataHeight as! Double
                        }
                        if(index.key == "href"){
                            href = index.value as! String
                        }
                        if(index.key == "key"){
                            key = index.value as! String
                        }
                    }
                    
                    DispatchQueue.main.async {
                        
                        guard let url = URL(string: url) else { return }
                        
                        let task = URLSession.shared.dataTask(with: url) { data, response, error in
                            guard let data = data else { return }
                            DispatchQueue.main.async {
                                self.width = width
                                self.height = height
                                self.href = href
                                self.data = data
                            }
                        }
                        Impression(adServerKey: key, appKey: appKey, adKey: adKey){ (result, error) in
                            if((error) != nil){
                                print(error as Any)
                            }
                        }
                        task.resume()
                    }
                } else if let error = error {
                    print("error: \(error.localizedDescription)")
                }
            }
        }
    }
}

